============Summary RandomForestNoMeanPCA8with_imputation============
Accuracy Score: 0.7633129852593675
Micro Recall Score: 0.7633129852593675
Macro Recall Score: 0.5067888625742775
Weighted Recall Score: 0.7633129852593675
Micro Precision Score: 0.7633129852593675
Macro Precision Score: 0.4269234886138092
Weighted Precision Score: 0.6210129986415764
Micro F1 Score: 0.7633129852593675
Macro F1 Score: 0.4588262316132462
Weighted F1 Score: 0.6823028001377993
================================
