============Summary RandomForestNoMeanPCA8without_imputation============
Accuracy Score: 0.7439251177783288
Micro Recall Score: 0.7439251177783288
Macro Recall Score: 0.6002277715314999
Weighted Recall Score: 0.7439251177783288
Micro Precision Score: 0.7439251177783288
Macro Precision Score: 0.518572615735915
Weighted Precision Score: 0.6938275063289981
Micro F1 Score: 0.7439251177783288
Macro F1 Score: 0.5447807480938756
Weighted F1 Score: 0.705934592266658
================================
