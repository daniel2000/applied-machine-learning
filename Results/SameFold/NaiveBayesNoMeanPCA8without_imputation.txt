============Summary NaiveBayesNoMeanPCA8without_imputation============
Accuracy Score: 0.8200471113315151
Micro Recall Score: 0.8200471113315151
Macro Recall Score: 0.7058013928513259
Weighted Recall Score: 0.8200471113315151
Micro Precision Score: 0.8200471113315151
Macro Precision Score: 0.7402281174397417
Weighted Precision Score: 0.8097501949201849
Micro F1 Score: 0.8200471113315151
Macro F1 Score: 0.7197574096558714
Weighted F1 Score: 0.8131242453673695
================================
